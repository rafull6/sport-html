<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="g4FK2Nqp4hcPx7RiAExofaqwSezaQ6CU-llUqDttdpc" />
<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<title><?php wp_title(); ?></title>
<link href="http://livedemo00.template-help.com/opencart_59089/image/catalog/favicon.png" rel="icon">
	<?php wp_head(); ?>
	<script>
		(function($){
			$(document).ready(function(){
				var example = $('#drop-menu').superfish({
				});
				$('.destroy').on('click', function(){
					example.superfish('destroy');
				});
				$('.init').on('click', function(){
					example.superfish();
				});

				$('.open').on('click', function(){
					example.children('li:first').superfish('show');
				});

				$('.close').on('click', function(){
					example.children('li:first').superfish('hide');
				});
			});
		})(jQuery);
	</script>
</head>

<body class="common-home">
<div id="page">
	<header>
		<?php
		$phone = get_field("phone", 188);
		$email = get_field("email", 188);
		?>
		<?php if(!empty($phone)) :?>
		<div class="m-contacts">
			<a href="tel:<?php echo $phone ?>" class="phone m">
				<span style="margin-left: 5px; text-decoration: underline;"><?php echo $phone ?></span>
			</a>
		</div>
		<?php endif; ?>
		<div class="top-line">
			<div class="container">
				<div class="cont">
					<div class="top-header">
						<?php if(!empty($phone) || !empty($email)) : ?>
						<a href="mailto:<?php echo $email ?>" class="email">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/images/mail.png" width="16px">
							<span style="margin-left: 5px;"><?php echo $email ?></span>
						</a>
						<a href="tel:<?php echo $phone ?>" class="phone">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/images/phone.png" width="16px">
							<span style="margin-left: 5px;"><?php echo $phone ?></span>
						</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div id="stuck" class="stuck-menu">
			<div class="container">
				<h1 id="logo" class="logo">
					<?php $url = home_url(); ?>
					<a href="<?php echo esc_url( $url ); ?>">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/images/logo.png" title="Кожаный мяч" alt="Кожаный мяч" class="logotype">
					</a>
				</h1>
			</div>
			<div class="navigation">
				<div class="megamenu">
					<?php sport_header_main_menu(); ?>
				</div>
			</div>
		</div>
	</header>

	<div class="mobile-menu">
		<ul id="menu">
			<?php sport_header_main_menu_mobile(); ?>
		</ul>
	</div>

	<?php if(is_front_page()) : ?>
	<section class="top">
		<div class="swiper-container swiper-slider swiper-container-horizontal swiper-container-fade">
			<?php if ( function_exists( 'meteor_slideshow' ) ) { meteor_slideshow(); } ?>
		</div>
	</section>
	<?php endif; ?>
	<section class="mid">
		<div class="container">


