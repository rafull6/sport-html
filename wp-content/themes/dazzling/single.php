<?php get_header(); ?>

<?php
global $user_ID;
//$cat = $_GET['cat'];

if(get_field('post_type_box') == 'p_ball'){
	if(in_category(9) || in_category(11) || in_category(4)) {
		echo '<div id="primary" class="content-area col-sm-12 col-md-8">'
			. '<main id="main" class="site-main" role="main">' . get_template_part('content', 'single')
			. '</main>'
			. '</div>';
	}
}elseif(get_field('post_type_box') == 'p_article'){
	if(in_category(9) || in_category(11) || in_category(4) || in_category(12)) {
		echo '<div id="primary" class="content-area col-sm-12 col-md-8">'
			. '<main id="main" class="site-main" role="main">' . get_template_part('content', 'article')
			. '</main>'
			. '</div>';
	}
}elseif(get_field('post_type_box') == 'p_ball_article'){
	if(in_category(9) || in_category(11) || in_category(4)) {
		echo '<div id="primary" class="content-area col-sm-12 col-md-8">'
			. '<main id="main" class="site-main" role="main">' . get_template_part('content', 'single')
			. '</main>'
			. '</div>';
	}
}elseif(get_field('post_type_box') == 'p_article_buy'){
	if(in_category(9) || in_category(11) || in_category(4) || in_category(12)) {
		echo '<div id="primary" class="content-area col-sm-12 col-md-8">'
			. '<main id="main" class="site-main" role="main">' . get_template_part('content', 'article')
			. '</main>'
			. '</div>';
	}
}
/*
if(in_category(9)){
	echo '<div id="primary" class="content-area col-sm-12 col-md-8">'
		. '<main id="main" class="site-main" role="main">'
		. get_template_part('content', 'single')
		. '</main></div>';
}elseif(in_category(11) || in_category(4)){
	echo '<div id="primary" class="content-area col-sm-12 col-md-8">'
		. '<main id="main" class="site-main" role="main">' . get_template_part( 'content', 'single' )
		. '</main>'
		. '</div>';
}elseif(in_category(12)){
	echo '<div id="primary" class="content-area col-sm-12 col-md-8">'
		. '<main id="main" class="site-main" role="main">' . get_template_part( 'content', 'article' )
		. '</main>'
		. '</div>';
}*/
?>

<?php get_footer(); ?>
