<?php
$_SESSION['visitedPages'][] .= get_the_title();
$_SESSION['lastVisitedPage'] = get_the_title();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container">
        <div class="form-group">
            <div id="content" class="col-sm-12">
                <div class="form-group">
                    <article class="article-info">
                        <div class="article-main-title second">
                            <h1><?php the_title(); ?></h1>
                        </div>
                        <div class="article-image article-single second">
                                <?php
                                $main_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                $new_image = get_field('bottom_examples_img');
                                if(!empty($new_image)){
                                    echo '<div id="banner0 nofloat" class="banners">'
                                         .'<div class="banner1 fullwidth centered">'
                                         .'<div class="banner-box">';
                                    if(get_field('post_type_box')=='p_article_buy'){
                                        echo '<a href="' . get_permalink(142) . '" class="product-btn-add lgreen-btn abs">Сделать заказ</a>';
                                    }
                                    echo '<a class="clearfix" href="#">'
                                        .'<img src="' . $new_image['url'] . '" class="img-responsive" >'
                                        .'<div class="layer-blue"></div>'
                                        .'</a></div></div></div>';
                                }else{
                                    echo '<div id="banner0 nofloat" class="banners">'
                                        .'<div class="banner1 fullwidth centered">'
                                        .'<div class="banner-box">';
                                    if(get_field('post_type_box')=='p_article_buy'){
                                        echo '<a href="' . get_permalink(142) . '" class="product-btn-add lgreen-btn abs">Сделать заказ</a>';
                                    }
                                    echo '<a class="clearfix" href="#">'
                                        .'<img src="' . $main_image . '" class="img-responsive" >'
                                        .'<div class="layer-blue"></div>'
                                        .'</a></div></div></div>';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="article-description col-lg-8 second">
                            <p>
                                <?php
                                while ( have_posts() ) {
                                    the_post();
                                    the_content();
                                }
                                ?>
                                <?php
                                if(get_field('post_type_box')=='p_article_buy'){
                                    echo '<a href="' . get_permalink(142) . '" class="product-btn-add green-btn" style="margin-right: 20px">Сделать заказ</a>';
                                }
                                ?>

                            </p>
                        </div>
                        <?php wp_reset_query(); ?>
                        <?php get_sidebar('sidebar-1'); ?>
                    </article>
                    <div class="container">

                        <ul class="breadcrumb">
                            <li class="holder"><a href="<?php /*echo get_home_url(); */?>">Главная</a></li>
                            <li><span><?php the_title(); ?></span></li>
                            <?php if( $user_ID ) {
                                if (current_user_can('level_10')) {
                                    echo '<li class="f-right">';
                                    echo edit_post_link('Редактировать', '<span>', '</span>', '', 'edit-link');
                                    echo '</li>';
                                }
                            }?>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>


</article>