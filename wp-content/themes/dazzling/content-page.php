<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container">
        <div class="form-group">
            <div id="content" class="col-sm-12">
                <div class="form-group">
                    <article class="article-info">
                        <div class="article-main-title">
                            <h1><?php the_title(); ?></h1>
                        </div>
                        <div class="article-description">
                            <p>
                                <?php the_content(); ?>
                            </p>
                        </div>
                    </article>

                    <ul class="breadcrumb">
                        <li class="holder"><a href="<?php echo get_home_url(); ?>">Главная</a></li>
                        <li><span><?php the_title(); ?></span></li>
                        <?php if ($user_ID) {
                            if (current_user_can('level_10')) {
                                echo '<li class="f-right">';
                                echo edit_post_link('Редактировать', '<span>', '</span>', '', 'edit-link');
                                echo '</li>';
                            }
                        } ?>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</article>

