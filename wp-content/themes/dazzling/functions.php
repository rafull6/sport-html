<?php
function register_my_session(){
    if( !session_id() ){
        session_start();
    }
}
add_action('init', 'register_my_session');

if ( ! isset( $content_width ) ) {
  $content_width = 730;
}

function dazzling_content_width() {
  if ( is_page_template( 'page-fullwidth.php' ) || is_page_template( 'front-page.php' ) ) {
    global $content_width;
    $content_width = 1110;
  }
}
add_action( 'template_redirect', 'dazzling_content_width' );

function dazzling_scripts() {

    //Styles CSS
    wp_enqueue_style( 'dazzling-bootstrap', get_template_directory_uri() . '/inc/css/bootstrap.min.css' );
    wp_enqueue_style( 'slicknav', get_template_directory_uri() . '/inc/css/slicknav.css' );
    wp_enqueue_style( 'dazzling-icons', get_template_directory_uri(). '/inc/css/font-awesome.min.css' );
    wp_enqueue_style( 'dazzling-fancybox', get_template_directory_uri(). '/inc/css/jquery.fancybox.css' );

    //Header js scripts
    wp_enqueue_style( 'dazzling-style', get_stylesheet_uri() );
    wp_enqueue_script('dazzling-bootstrapjs', get_template_directory_uri().'/inc/js/bootstrap.min.js', array('jquery') );
    //wp_enqueue_script('dazzling-ui', get_template_directory_uri().'/inc/js/jquery-ui.min.js', array('jquery') );
    //wp_enqueue_script( 'jquery-2.1.1.min', get_template_directory_uri() . '/inc/js/jquery-2.1.1.min.js', array('jquery'), '2.5.0', false );
    wp_enqueue_script( 'superfish.min', get_template_directory_uri() . '/inc/js/superfish.min.js', array('jquery'), '2.5.0', false );

    //Footer js scripts
    wp_enqueue_script( 'jquery.slicknav.min', get_template_directory_uri() . '/inc/js/jquery.slicknav.min.js', array('jquery'), '2.5.0', true );
    wp_enqueue_script( 'jquery.easing.1.3', get_template_directory_uri() . '/inc/js/jquery.easing.1.3.js', array('jquery'), '2.5.0', true );
    wp_enqueue_script( 'script', get_template_directory_uri() . '/inc/js/script.js', array('jquery'), '2.5.0', true );
    wp_enqueue_script( 'jquery.fancybox', get_template_directory_uri() . '/inc/js/jquery.fancybox.js', array('jquery'), '2.5.0', true );
}

add_action( 'wp_enqueue_scripts', 'dazzling_scripts' );

/**
 * Add HTML5 shiv and Respond.js for IE8 support of HTML5 elements and media queries
 */
function dazzling_ie_support_header() {
  echo '<!--[if lt IE 9]>'. "\n";
  echo '<script src="' . esc_url( get_template_directory_uri() . '/inc/js/html5shiv.min.js' ) . '"></script>'. "\n";
  echo '<script src="' . esc_url( get_template_directory_uri() . '/inc/js/respond.min.js' ) . '"></script>'. "\n";
  echo '<![endif]-->'. "\n";
}
add_action( 'wp_head', 'dazzling_ie_support_header', 11 );

require get_template_directory() . '/inc/extras.php';

/* Globals variables */
global $options_categories;
$options_categories = array();
$options_categories_obj = get_categories();
foreach ($options_categories_obj as $category) {
        $options_categories[$category->cat_ID] = $category->cat_name;
}

global $site_layout;
$site_layout = array('side-pull-left' => esc_html__('Right Sidebar', 'dazzling'),'side-pull-right' => esc_html__('Left Sidebar', 'dazzling'),'no-sidebar' => esc_html__('No Sidebar', 'dazzling'),'full-width' => esc_html__('Full Width', 'dazzling'));

// Typography Options
global $typography_options;
$typography_options = array(
        'sizes' => array( '6px' => '6px','10px' => '10px','12px' => '12px','14px' => '14px','15px' => '15px','16px' => '16px','18px'=> '18px','20px' => '20px','24px' => '24px','28px' => '28px','32px' => '32px','36px' => '36px','42px' => '42px','48px' => '48px' ),
        'faces' => array(
                'arial'          => 'Arial,Helvetica,sans-serif',
                'verdana'        => 'Verdana,Geneva,sans-serif',
                'trebuchet'      => 'Trebuchet,Helvetica,sans-serif',
                'georgia'        => 'Georgia,serif',
                'times'          => 'Times New Roman,Times, serif',
                'tahoma'         => 'Tahoma,Geneva,sans-serif',
                'Open Sans'      => 'Open Sans,sans-serif',
                'palatino'       => 'Palatino,serif',
                'helvetica'      => 'Helvetica,Arial,sans-serif',
                'helvetica-neue' => 'Helvetica Neue,Helvetica,Arial,sans-serif'
        ),
        'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
        'color'  => true
);

// Typography Defaults
global $typography_defaults;
$typography_defaults = array(
        'size'  => '14px',
        'face'  => 'helvetica-neue',
        'style' => 'normal',
        'color' => '#6B6B6B'
);

/**
 * Helper function to return the theme option value.
 * If no value has been saved, it returns $default.
 * Needed because options are saved as serialized strings.
 *
 * Not in a class to support backwards compatibility in themes.
 */
if ( ! function_exists( 'of_get_option' ) ) :
function of_get_option( $name, $default = false ) {

  $option_name = '';
  // Get option settings from database
  $options = get_option( 'dazzling' );

  // Return specific option
  if ( isset( $options[$name] ) ) {
    return $options[$name];
  }

  return $default;
}
endif;

add_theme_support( 'post-thumbnails' );

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
    } else {
        $content = implode(" ",$content);
    }
    $content = preg_replace('/\[.+\]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Info sidebar' ),
        'id' => 'sidebar-1',
        'description' => __( 'Sidebar with prices and other data' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s sidebar-zone first">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}

class sdata_widget extends WP_Widget {
    function __construct() {
        parent::__construct(
            'sdata_widget',
            __('Sidebar page data', 'sdata_widget_domain'),
            array( 'description' => __( '', 'sdata_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {
        $sidebar_price = get_field('sidebar_price');
        $sidebar_quantity = get_field('sidebar_quantity');

        $art_img_1 = get_field('art_img_1');
        $art_img_2 = get_field('art_img_2');
        $art_img_3 = get_field('art_img_3');
        $art_img_4 = get_field('art_img_4');
        $art_img_5 = get_field('art_img_5');

        $art_desc_1 = get_field('art_desc_1');
        $art_desc_2 = get_field('art_desc_2');
        $art_desc_3 = get_field('art_desc_3');
        $art_desc_4 = get_field('art_desc_4');
        $art_desc_5 = get_field('art_desc_5');

        echo $args['before_widget'];
        echo '<div class="holder">'
            .'<div class="half left-border">'
            .'<p class="header">Цена:</p>'
            .'<p class="data">' . $sidebar_price . '&#x20bd;</p>'
            .'</div>';
        echo '<div class="half">'
            .'<p class="header">Тираж:</p>'
            .'<p class="data">' . $sidebar_quantity . '</p>'
            .'</div>'
            .'</div>';
        echo '<span style="font-size: 18px; width: 100%; background-color: #1D8295; color: #fff; display: block; text-align: center; margin-top: 20px; padding: 10px 0;">Примеры работ:</span>';

        if(!empty($art_img_1)){
            echo '<div class="flip-container" ontouchstart="this.classList.toggle(\'hover\');">'
                .'<div class="image">'
                .'<div class="front"><img src="' . $art_img_1['url'] . '"></div>'
                .'<div class="back"><span>' . $art_desc_1 . '</span></div>'
                .'</div>'
                .'</div>';
        }
        if (!empty($art_img_2)){
            echo '<img class="image" src="' . $art_img_2['url'] . '">';
        }
        if (!empty($art_img_3)){
            echo '<img class="image" src="' . $art_img_3['url'] . '">';
        }
        if (!empty($art_img_4)){
            echo '<img class="image" src="' . $art_img_4['url'] . '">';
        }
        if (!empty($art_img_5)){
            echo '<img class="image" src="' . $art_img_5['url'] . '">';
        }
        echo $args['after_widget'];
    }
}

function sdata_load_widget() {
    register_widget( 'sdata_widget' );
}
add_action( 'widgets_init', 'sdata_load_widget' );

class ymaps_widget extends WP_Widget {
    function __construct() {
        parent::__construct(
            'ymaps_widget',
            __('Yandex maps', 'ymaps_widget_domain'),
            array( 'description' => __( '', 'ymaps_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {

        echo '';

    }

}

function ymaps_load_widget() {
    register_widget( 'ymaps_widget' );
}
add_action( 'widgets_init', 'ymaps_load_widget' );


/* DEBUG */
function console_log( $data ){
    echo '<script>';
    echo 'console.log("'. $data .'")';
    echo '</script>';
}
