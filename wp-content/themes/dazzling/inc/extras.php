<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package dazzling
 */

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * @param array $args Configuration arguments.
 * @return array
 */
function dazzling_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'dazzling_page_menu_args' );


/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function dazzling_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter( 'body_class', 'dazzling_body_classes' );


/**
 * Mark Posts/Pages as Untiled when no title is used
 */
add_filter( 'the_title', 'dazzling_title' );

function dazzling_title( $title ) {
  if ( $title == '' ) {
    return 'Untitled';
  } else {
    return $title;
  }
}


/**
 * Add Filters
 */
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar


/**
 * Password protected post form
 */
add_filter( 'the_password_form', 'custom_password_form' );

function custom_password_form() {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$o = '<form class="protected-post-form" action="' . get_option('siteurl') . '/wp-login.php?action=postpass" method="post">
  <div class="row">
    <div class="col-lg-10">
        <p>' . __( "This post is password protected. To view it please enter your password below:" ,'dazzling') . '</p>
        <label for="' . $label . '">' . __( "Password:" ,'dazzling') . ' </label>
      <div class="input-group">
        <input class="form-control" value="' . get_search_query() . '" name="post_password" id="' . $label . '" type="password">
        <span class="input-group-btn"><button type="submit" class="btn btn-default" name="submit" id="searchsubmit" vvalue="' . esc_attr__( "Submit",'dazzling' ) . '">' . __( "Submit" ,'dazzling') . '</button>
        </span>
      </div>
    </div>
  </div>
</form>';
	return $o;
}


/**
 * Add Bootstrap classes for table
 */
add_filter( 'the_content', 'dazzling_add_custom_table_class' );
function dazzling_add_custom_table_class( $content ) {
  return str_replace( '<table>', '<table class="table table-hover">', $content );
}

if ( ! function_exists( 'dazzling_social_icons' ) ) :
/**
 * Display social links in footer and widgets
 */
function dazzling_social_icons(){
  if ( has_nav_menu( 'social-menu' ) ) {
  	wp_nav_menu(
      array(
        'theme_location'  => 'social-menu',
        'container'       => 'nav',
        'container_id'    => 'social',
        'container_class' => 'social-icon',
        'menu_id'         => 'menu-social-items',
        'menu_class'      => 'social-menu',
        'depth'           => 1,
        'fallback_cb'     => '',
        'link_before'     => '<i class="social_icon fa"><span>',
        'link_after'      => '</span></i>'
      )
    );
  }
}
endif;


if( !function_exists( 'dazzling_social' ) ) :
	/**
	 * Fallback function for the deprecated function dazzling_social
	*/
function dazzling_social(){
  if( of_get_option('footer_social') ) {
    dazzling_social_icons();
  }
}
endif;

/**
 * header TOP menu
 */
function sport_header_top_menu() {
  wp_nav_menu(array(
      'menu'              => 2,
      'theme_location'    => 2,
      'container'         => '',
      'container_class'   => '',
      'menu_class'        => 'toggle_cont',
  ));
} /* end of TOP menu */

/**
 * header MAIN menu
 */
function sport_header_main_menu() {
    wp_nav_menu(array(
        'menu'              => 3,
        'theme_location'    => 3,
        'menu_id'           => 'drop-menu',
        'container'         => '',
        'container_class'   => '',
        'menu_class'        => 'sf-menu',
    ));
} /* end of MAIN menu */

function sport_header_main_menu_mobile() {
    wp_nav_menu(array(
        'menu'              => 3,
        'theme_location'    => 3,
        'menu_id'           => 'drop-menu-mobile',
        'container'         => '',
        'container_class'   => '',
    ));
} /* end of MAIN menu */


/**
 * footer menu (should you choose to use one)
 */
function dazzling_footer_links() {
  // display the WordPress Custom Menu if available
  wp_nav_menu(array(
    'container'       => '',                              // remove nav container
    'container_class' => 'footer-links clearfix',   // class of container (should you choose to use it)
    'menu'            => __( 'Footer Links', 'dazzling' ),   // nav name
    'menu_class'      => 'nav footer-nav clearfix',      // adding custom nav class
    'theme_location'  => 'footer-links',             // where it's located in the theme
    'before'          => '',                                 // before the menu
    'after'           => '',                                  // after the menu
    'link_before'     => '',                            // before each link
    'link_after'      => '',                             // after each link
    'depth'           => 0,                                   // limit the depth of the nav
    'fallback_cb'     => 'dazzling_footer_links_fallback'  // fallback function
  ));
} /* end dazzling footer link */


if ( ! function_exists( 'dazzling_footer_info' ) ) :
/**
 * function to show the footer info, copyright information
 */
function sport_footer_info() {
  global $dazzling_footer_info;
  printf( __('Sport © 2016'));
}
endif;

?>
