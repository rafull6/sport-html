/**
* @function      isIE
* @description   checks if browser is an IE
* @returns       {number} IE Version
*/
function isIE() {
	var myNav = navigator.userAgent.toLowerCase();
	return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
};

;(function ($) {
	if (isIE() && isIE() < 11) {
		$('html').addClass('lt-ie11');
	}
})(jQuery);

/* FancyBox
========================================================*/
;(function ($) {
	var o = $('.quickview');
	var o2 = $('#default_gallery');
	if (o.length > 0 || o2.length > 0) {
		include('js/fancybox/jquery.fancybox.js');
	}
	if (o.length) {
		$(document).ready(function () {
			o.fancybox({
				maxWidth: 800,
				maxHeight: 600,
				fitToView: false,
				width: '70%',
				height: '70%',
				autoSize: false,
				closeClick: false,
				openEffect: 'elastic',
				closeEffect: 'elastic',
				speedIn: 600,
				speedOut: 600
			});
		});
	}
})(jQuery);

/* Breadcrumb Last element replacement
========================================================*/
;(function ($) {
	$(document).ready(function () {
		if ($('.breadcrumb').length) {
			var o = $('.breadcrumb li:last-child > a');
			o.replaceWith('<span>' + o.html() + '</span>');
		}
	});
})(jQuery);
