<?php get_header(); ?>

<section id="primary" class="content-area<?php echo of_get_option('site_layout'); ?>">
    <main id="main" class="site-main" role="main">
        <?php
        $cl = $_GET['cl'];

        if (!empty($cl)) {
            if ($cl == 1) {
                if (in_category(4) || in_category(11)) {
                    if (have_posts()) {
                        ?>
                        <div class="article-main-title">
                            <h1><?php /*single_cat_title() */?></h1>
                        </div>
                        <?php
                        while (have_posts()) {
                            the_post();
                            get_template_part('products');
                        }
                    }
                }
            } elseif ($cl == 2) {
                if (in_category(4) || in_category(11)) {
                    if (have_posts()) { ?>
                        <div class="article-main-title">
                            <h1><?php /*single_cat_title() */?></h1>
                        </div>
                        <?php
                        while (have_posts()) {
                            the_post();
                            get_template_part('products', get_post_format());
                        }
                    }
                }
            } elseif ($cl == 3) {
                if (in_category(9)) {
                    if (have_posts()) { ?>
                        <div class="article-main-title">
                            <h1><?php single_cat_title() ?></h1>
                        </div>
                        <?php
                        while (have_posts()) {
                            the_post();
                            get_template_part('content', get_post_format());
                        }
                    }
                }
            }
        } else {
            if (in_category(4) || in_category(11)) {
                if (have_posts()) {
                    ?>
                    <div class="article-main-title">
                        <h1><?php single_cat_title() ?></h1>
                    </div>
                    <?php
                    while (have_posts()) {
                        the_post();
                        get_template_part('products');
                    }
                }
            } elseif (in_category(9)) {
                if (have_posts()) { ?>
                    <div class="article-main-title">
                        <h1><?php single_cat_title() ?></h1>
                    </div>
                    <?php
                    while (have_posts()) {
                        the_post();
                        get_template_part('content', get_post_format());
                    }
                }
            }
        }
        ?>
    </main>
</section>

<?php get_footer(); ?>
