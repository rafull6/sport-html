<?php get_header(); ?>
	<div id="primary" class="content-area col-sm-12 col-md-8">
		<main id="main" class="site-main" role="main">
			<?php
			$id = get_the_ID();
			while ( have_posts() ) {
				the_post();

				if($id != 142){
					get_template_part( 'content', 'page' );
				}else{
					get_template_part( 'content', 'feedback' );
				}
			}
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
