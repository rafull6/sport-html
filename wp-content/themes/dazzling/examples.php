<div class="col-sm-6 col-sx-12">
    <a href="<?php the_permalink(); ?>">
        <figure class="article-image examples">
            <?php if (has_post_thumbnail()) {
                $main_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                $new_image = get_field('bottom_examples_img');

                if(!empty($new_image)){
                    echo '<img src="' . $new_image['url'] . '">';
                }else{
                    echo '<img src="' . $main_image . '">';
                }
            } ?>
            <div class="article-caption">
                <div class="article-title ">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </figure>
    </a>
</div>
