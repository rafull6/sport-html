<?php
$_SESSION['visitedPages'][] .= get_the_title();
$_SESSION['lastVisitedPage'] = get_the_title();
$post_id = get_the_ID();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12 margin20">
                <div class="row">
                    <div class="col-sm-6 col-lg-6 product_page-left">
                        <div class="product-gallery">
                            <div class="row">
                                <div class="col-lg-12 pull-right text-center">
                                    <?php
                                    $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                                    if(!empty($image)){
                                        echo '<div class="image-box"><img src="'. $image .'"></div>';
                                        echo '<div class="image-box-mobile"><img src="'. $image .'"></div>';
                                    }
                                    ?>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-lg-6 product_page-right">
                        <div class="general_info product-info">
                            <h1 class="product-title">
                                <?php
                                $ball_prod = get_field('firm_prod');
                                $ball_name = get_field('name_soccer_ball');
                                $ball_model = get_field('ball_model');
                                if(!empty($ball_model) || !empty($ball_prod)){
                                    echo $ball_prod . '<br />' . $ball_name . '<br />' . $ball_model;
                                }elseif (empty($ball_model) && empty($ball_prod)){
                                    $title = get_the_title();
                                    $firts_to_words = preg_match('/^(?>\S+\s*){1,2}/', $title, $match);
                                    echo rtrim($match[0]);

                                    $all_words = preg_replace('/^(?>\S+\s*){1,2}/', "", $title);
                                    echo '<br />' . $all_words;
                                }
                                ?>

                            </h1>
                            <?php
                            $price = get_field('price', false, false);
                            $old_price = get_field('old_price', false, false);
                            $opt_price = get_field('opt_price', false, false);
                            $minimal_amount = get_field('minimal_amount', false, false);
                            $availability = get_field('availability', false, false);

                            if(!empty($price)){
                                echo '<div class="price-section">';
                                echo '<span style="font-size: 26px">Цена: </span>';
                                if(!empty($old_price)){
                                    echo'<span class="price-new" style="color: #0C7406">' . $price . '</span>';
                                    echo '<span class="price-old" style="color: #777; margin-left: 5px;">' . $old_price . '</span>';
                                }else{
                                    echo'<span class="price-new">' . $price . '</span>';
                                }
                                echo '</div>';
                            }

                            if(!empty($opt_price)){
                                echo '<div class="product-section">'
                                    .'<strong>Оптовая цена: </strong>'
                                    .'<span class="stock">' . $opt_price . '</span></div>';
                            }
                            if(!empty($minimal_amount)){
                                echo '<div class="product-section">'
                                    .'<strong>Минимальный тираж: </strong>'
                                    .'<span class="stock">' . $minimal_amount . '</span></div>';
                            }
                            if(!empty($availability)){
                                echo '<div class="product-section">'
                                    .'<strong>Доступность: </strong>'
                                    .'<span class="stock">' . $availability . '</span></div>';
                            }
                            ?>

                            <div class="product-data">
                                <?php
                                $size = get_field('size');
                                $layers = get_field('layers');
                                $print = get_field('print');
                                $grid = get_field('grid');
                                $material = get_field('material');
                                $stitching = get_field('stitching');
                                $camera = get_field('camera');
                                $weight	 = get_field('weight');

                                echo '<div class="details-table">
                                    <div class="details-row">
                                        <div class="details-cell-small">Размер</div>
                                        <div class="details-cell-big">' . $size . '</div>
                                    </div>
    
                                    <div class="details-row">
                                        <div class="details-cell-small">Слоев</div>
                                        <div class="details-cell-big">' . $layers . '</div>
                                    </div>
        
                                    <div class="details-row">
                                        <div class="details-cell-small">Печать</div>
                                        <div class="details-cell-big">' . $print . '</div>
                                    </div>
        
                                    <div class="details-row">
                                        <div class="details-cell-small">Сетка</div>
                                        <div class="details-cell-big">' . $grid . '</div>
                                    </div>
                                </div>';

                                        echo '<div class="details-table">
                                    <div class="details-row">
                                        <div class="details-cell-small">Материал</div>
                                        <div class="details-cell-big">' . $material . '</div>
                                    </div>
        
                                    <div class="details-row">
                                        <div class="details-cell-small">Сшивка</div>
                                        <div class="details-cell-big">' . $stitching . '</div>
                                    </div>
        
                                    <div class="details-row">
                                        <div class="details-cell-small">Камера</div>
                                        <div class="details-cell-big">' . $camera . '</div>
                                    </div>
        
                                    <div class="details-row">
                                        <div class="details-cell-small">Вес</div>
                                        <div class="details-cell-big">' . $weight . '</div>
                                    </div>
                                </div>';
                                ?>
                            </div>

                            <div id="product">
                                <a href="<?php echo get_permalink(142); ?>" class="product-btn-add green-btn">Сделать заказ</a>
                                <a href="<?php echo get_permalink(142); ?>" class="product-btn-add">Связаться с нами</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            $post = get_post();
            if ( !empty($post->post_content)) : ?>
                <div class="product_tabs">
                    <div class="box-heading">
                        <h3>Описание</h3>
                    </div>
                    <div class="tab-content single-content">
                        <div class="tab-pane active" id="tab-description">
                            <p>
                                <?php
                                while ( have_posts() ) {
                                    the_post();
                                    the_content();
                                }
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <?php
        $categories = get_the_category();
        if (!empty($categories)){
            echo '<div class="box-heading">'
                .'<h3>Похожие материалы</h3>'
                .'</div>';
            echo '<div class="offers-single">';
            $post_query = new WP_Query(array('post_status'=>'publish', 'posts_per_page'=>4, 'orderby'=>'rand', 'category__not_in' => array( 1, 12 )));
            if ( $post_query->have_posts() ) {
                while ($post_query->have_posts()) {
                    $post_query->the_post();
                    get_template_part('products');
                }
            }
            echo '</div>';
        }
        ?>
        <ul class="breadcrumb">
            <li class="holder"><a href="<?php echo get_home_url(); ?>">Главная</a></li>
            <li><span>
            <?php 
            echo get_the_title($post_id);
            ?>
            </span></li>

            <?php if( $user_ID ) {
                if (current_user_can('level_10')) {
                    echo '<li class="f-right">';
                    echo edit_post_link('Редактировать', '<span>', '</span>', '', 'edit-link');
                    echo '</li>';
                }
            }?>
        </ul>
    </div>
</article>
