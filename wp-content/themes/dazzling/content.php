<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container">
        <div class="form-group">
            <div id="content" class="col-sm-12">
                <div class="form-group">
                </div>
                <article class="blog_article">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <figure class="article-image">
                                <?php
                                $main_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                                echo '<img src="' . $main_image . '">';
                                ?>
                            </figure>
                        </div>
                        <div class="col-sm-12 col-md-6 list-content">
                            <h2 class="article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <div class="article-description"><?php echo excerpt(50); ?></div>
                            <div>
                                <a class="btn" href="<?php the_permalink(); ?>">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</div>
