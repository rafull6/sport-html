<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container">
        <div class="form-group">
            <div id="content" class="col-sm-12">
                <div class="form-group">
                    <article class="article-info">
                        <div class="article-main-title">
                            <h1><?php the_title(); ?></h1>
                        </div>
                        <div class="article-description feedback-description">
                            <p>
                                <?php the_content(); ?>
                            </p>
                        </div>
                        <div class="article-feedback-widget-block">
                            <?php echo do_shortcode('[contact-form-7 id="647" title="Contact form 1"]'); ?>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <ul class="breadcrumb">
            <li class="holder"><a href="<?php echo get_home_url(); ?>">Главная</a></li>
            <li><span><?php the_title(); ?></span></li>
            <?php if( $user_ID ) {
                if (current_user_can('level_10')) {
                    echo '<li class="f-right">';
                    echo edit_post_link('Редактировать', '<span>', '</span>', '', 'edit-link');
                    echo '</li>';
                }
            }?>
        </ul>
    </div>
</article>


<?php
if(!empty($_SESSION['visitedPages']) || !empty($_SESSION['lastVisitedPage'])){
    echo '<script>'
        . 'var visitedPagesJSON = ' . json_encode($_SESSION['visitedPages']) . ';'
        . 'document.getElementById("last-item").value = "' . $_SESSION['lastVisitedPage'] . '";'
        . 'document.getElementById("visited-items").value = visitedPagesJSON.join("\n");'
        .'</script>';
}

echo '<script>'
     .'jQuery( "#send" ).click(function() {'
     .'jQuery.get("clearsession.php", function() {'
     .'alert("the server page executed");});'
     .'});</script>'
?>
