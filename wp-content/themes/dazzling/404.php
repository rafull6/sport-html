<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package dazzling
 */

get_header(); ?>
		<div id="primary" class="content-area col-sm-12">
			<main id="main" class="site-main" role="main">

				<section class="error-404 not-found">
					<div class="page-content">
						<span class="code">404</span>
						<p class="title">Страница не найдена :(</p>
						<a class="back" href="<?php echo get_home_url(); ?>">Вернуться на главную страницу</a>
					</div><!-- .page-content -->
				</section><!-- .error-404 -->

			</main><!-- #main -->
		</div><!-- #primary -->

<?php get_footer(); ?>