<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12">
    <div class="product-thumb tiels" data-match-height="match">
        <?php
        $opt_price = get_field('opt_price', false, false);
        $price = get_field('price', false, false);
        $old_price = get_field('old_price', false, false);

        ?>
        <div class="name">
            <h1><a href="<?php the_permalink(); ?>">
            <?php 
                $ball_prod = get_field('firm_prod');
                $ball_name = get_field('name_soccer_ball');
                $ball_model = get_field('ball_model');
                if(!empty($ball_model) || !empty($ball_prod)){
                    echo $ball_prod . '<br />' . $ball_name . '<br />' . $ball_model;
                }elseif (empty($ball_model) && empty($ball_prod)){
                    $title = get_the_title();
                    echo $title;
                }
            ?>
            </a></h1>
        </div>


        <div class="image home">
            <a href="<?php the_permalink(); ?>">
                <?php
                if (has_post_thumbnail()) {
                    echo '<img src="' . the_post_thumbnail() . '">';
                }
                ?>
            </a>
        </div>
        <?php if(!empty($price) || !empty($old_price) || !empty($opt_price)){
            echo '<div class="prices-holder">';
            if (!empty($price)){
                echo '<div class="element">
                          <div class="title">Цена: </div>
                          <div class="price">' . $price . '</div>
                      </div>';
            }
            if (!empty($old_price)) {
                echo '<div class="element">
                          <div class="title">Старая цена: </div>
                          <div class="price price-old">' . $old_price . '</div>
                      </div>';
            } else if (!empty($opt_price)) {
                $old_price = '';
                echo '<div class="element">
                          <div class="title">Опт. цена: </div>
                          <div class="price price-opt">' . $opt_price . '</div>
                      </div>';
            }
            echo '</div>';

            echo '<div class="prices-holder-mobile">';
            if (!empty($price)){
                echo '<div class="element">Цена: <span style="font-weight: bold; color: #1D8295">' . $price . '</span></div>';
            }
            if (!empty($old_price)) {
                echo '<div class="element">Старая цена: <span style="font-weight: bold; color: #555">' . $old_price . '</span></div>';
            } else if (!empty($opt_price)) {
                $old_price = '';
                echo '<div class="element">Опт. цена: <span style="font-weight: bold; color: #00981b">' . $opt_price . '</span></div>';
            }
            echo '</div>';
        }else{
            echo '<div class="prices-holder for-ordering"><span>На заказ</span></div>';
            echo '<div class="prices-holder-mobile for-ordering"><span>На заказ</span></div>';
        }
        ?>
    </div>
</div>