<?php get_header(); ?>
		<div class="row">
			<div id="content" class="col-sm-12">
				<div class="clearfix"></div>

				<!-- Dynamic content block -->
				<div class="module_tab module_tab__0">
					<div class="box-heading">
						<a href="<?php echo esc_url( get_category_link(4) ); ?>/?cl=1">Примеры работ</a>
					</div>
					<div class="tab-content">
						<div class="tab-pane active">
							<div class="row">
								<?php
								$post_query = new WP_Query(array('cat'=>4, 'post_status'=>'publish', 'posts_per_page'=>8));
								if ( $post_query->have_posts() ) {
									while ($post_query->have_posts()) {
										$post_query->the_post();
										get_template_part('products');
									}
								}?>
							</div>
							<a class="more" href="<?php echo esc_url( get_category_link(4) ); ?>/?cl=1">Больше примеров</a>
						</div>
					</div>
				</div>
				<!-- End of dynamic content block -->
				<div class="module_tab module_tab__0">
					<div class="box-heading">
						<a href="<?php echo esc_url( get_category_link(11) ); ?>/?cl=2">Брендовые мячи</a>
					</div>
					<div class="tab-content">
						<div class="tab-pane active">
							<div class="row">
								<?php
								$post_query = new WP_Query(array('cat'=>11, 'post_status'=>'publish', 'posts_per_page'=>8));
								if ( $post_query->have_posts() ) {
									while ($post_query->have_posts()) {
										$post_query->the_post();
										get_template_part('products');
									}
								}?>
							</div>
							<a class="more" href="<?php echo esc_url( get_category_link(11) ); ?>/?cl=2">Больше примеров</a>
						</div>
					</div>
				</div>
				<!-- End of dynamic content block -->
			</div>
		</div>
	</div>
</section>
<section class="content_bottom">
	<div class="container">
		<div class="box blog_articles">
			<div class="box-heading">
				<a href="<?php echo esc_url( get_category_link(9) ); ?>/?cl=3">Примеры работ</a>
			</div>
			<div class="box-content">
				<div class="row">
					<?php $examples_query = new WP_Query(array( 'cat' => 9, 'post_status'=>'publish', 'posts_per_page'=>2));
					if ( $examples_query->have_posts() ) {
						while ($examples_query->have_posts()) {
							$examples_query->the_post();
							get_template_part('examples');
							}
					}
					?>
				</div>
				<a class="more margin-top" href="<?php echo esc_url( get_category_link(9) ); ?>/?cl=3">Больше примеров</a>
			</div>
		</div>

<?php get_footer(); ?>
