<?php
/*
  Plugin Name: Yandex Map
  Plugin URI: http://vladysdovich.com
  Description: Яндекс карты для боковой панели.
  Version: 1.0.0
 */

if (!defined('ABSPATH')) {
    exit("Your don't have required permission.");
}

if (!class_exists('ymap_main')):

    class ymap_main {

        private $plugin_name = 'Yandex Map';

        function __construct() {
            add_action('wp_enqueue_scripts', array($this, 'ymap_enqueue_scripts'));
            add_action('admin_init', array($this, 'ymap_register_fields'));
        }

        public function ymap_enqueue_scripts() {
            wp_enqueue_script('ymap_api', 'https://api-maps.yandex.ru/2.1/?lang=ru_RU', array('jquery'));
        }

        public function ymap_register_fields() {
            register_setting('ymap_settings_group', 'ymap_title');
            register_setting('ymap_settings_group', 'ymap_lat');
            register_setting('ymap_settings_group', 'ymap_long');
            register_setting('ymap_settings_group', 'ymap_width');
            register_setting('ymap_settings_group', 'ymap_height');
            register_setting('ymap_settings_group', 'ymap_zoom');
            register_setting('ymap_settings_group', 'ymap_link');
        }
    }
    endif;

new ymap_main();
require_once plugin_dir_path(__FILE__) . '/includes/widget.php';
?>
