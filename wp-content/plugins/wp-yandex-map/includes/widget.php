<?php

class ymap_widget extends WP_Widget {

    public $base_id = 'ymap_widget'; 
    public $widget_name = 'Яндекс Карты';
    public $widget_options = array(
        'description' => 'Яндекс Карты'
    );

    public function __construct() {
        parent::__construct($this->base_id, $this->widget_name, $this->widget_options);
    }

    public function widget($args, $instance) {
        extract($args);
        extract($instance);

        $ymap_title = $title == '' ? get_option('ymap_title') : $title;
        $ymap_lat = $latitude == '' ? get_option('ymap_lat') : $latitude;
        $ymap_long = trim($longitude) == '' ? get_option('ymap_long') : $longitude;
        $ymap_width = $width == '' ? get_option('ymap_width') : $width;
        $ymap_height = $height == '' ? get_option('ymap_height') : $height;
        $ymap_zoom = $zoom == '' ? get_option('ymap_zoom') : $zoom;
        $ymap_link = $link == '' ? get_option('ymap_link') : $link;

        echo $before_widget
        ?>
        <script type="text/javascript">
            ymaps.ready(init);
            var mapDiv, map, marker;

            function init() {
                mapDiv = document.getElementById("ymp_<?php echo $this->id; ?>");
                map = new ymaps.Map(mapDiv, {
                    center: [<?php echo $ymap_lat; ?>, <?php echo $ymap_long; ?>],
                    zoom: <?php echo $ymap_zoom; ?>,
                    controls: []
                });
                marker = new ymaps.Placemark([<?php echo $ymap_lat; ?>, <?php echo $ymap_long; ?>]);
                map.geoObjects.add(marker);
                map.behaviors.disable('scrollZoom');
            }
        </script>
        <div class="yandex-map">
            <div id="ymp_<?php echo $this->id; ?>" style="width:<?php echo $ymap_width . '%'; ?>;height:<?php echo $ymap_height . 'px'; ?>;"></div>
        </div>
        <?php
        echo $widget_body_text
        . $after_widget;
    }

    public function form($instance) {
        ?>  
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Название карты (произвольное):</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" type="text" class="widefat">
        </p> 

        <p>
            <label for="<?php echo $this->get_field_id('latitude'); ?>">Широта (координаты):</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('latitude'); ?>" name="<?php echo $this->get_field_name('latitude'); ?>" value="<?php echo $instance['latitude']; ?>" type="text" class="widefat">
        </p>  

        <p>
            <label for="<?php echo $this->get_field_id('longitude'); ?>">Долгота (координаты):</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('longitude'); ?>" name="<?php echo $this->get_field_name('longitude'); ?>" value="<?php echo $instance['longitude']; ?>" type="text" class="widefat">
        </p>        

        <p>
            <label for="<?php echo $this->get_field_id('width'); ?>">Ширина в %:</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>" value="<?php echo $instance['width']; ?>" type="text" class="widefat">
        </p>        

        <p>
            <label for="<?php echo $this->get_field_id('height'); ?>">Высота в пикселях (по умолчанию 200):</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('height'); ?>" name="<?php echo $this->get_field_name('height'); ?>" value="<?php echo $instance['height']; ?>" type="text" class="widefat">
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('zoom'); ?>">Уровень приближения (по умолчанию 16):</label>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('zoom'); ?>" name="<?php echo $this->get_field_name('zoom'); ?>" value="<?php echo $instance['zoom']; ?>" type="text" class="widefat">
        </p>

        <?php
    }

}

add_action('widgets_init', create_function('', 'return register_widget("ymap_widget");'));
